(function(){
    "use strict";
    angular
        .module("pancakesProblem")
        .controller("PancakesCtrl",
            PancakesCtrl);

    function PancakesCtrl(){

        var viewModel = this;

        //initialize view model
        viewModel.numberOfTestCases = "";
        viewModel.pancakesTestCasesArray = [];
        viewModel.requiredFlipsArray = [];

        //controller functions
        viewModel.onCasesInputChange = function(){

            if(angular.isDefined(viewModel.pancakesTestCasesArray.length)){

                if(!isNaN(viewModel.numberOfTestCases) && viewModel.numberOfTestCases> 0){

                viewModel.pancakesTestCasesArray.length = viewModel.numberOfTestCases;
                }
            }
        };

        viewModel.onDirectionInputChange = function(index){

            viewModel.requiredFlipsArray.length = viewModel.pancakesTestCasesArray.length;
            viewModel.requiredFlipsArray[index] = 0;

            var stack =_getPancakeStackFromIndex(viewModel.pancakesTestCasesArray,index);

            stack = stack.split('');

            //trivial case
            if(stack.indexOf("-") === -1 ){
                viewModel.requiredFlipsArray[index] = 0;
            }

            else{
                var lengthOfStack = stack.length;

                //if the bottom pancake is face down, make all pancakes face down then flip from the bottom
                if(stack[lengthOfStack-1]=="-"){

                    //instead of starting from i-1 we start from i-2 since the first case is already accounted for
                    for(var i=lengthOfStack-2;i>=0;i--){

                        if(stack[i]=="+"){
                            stack = _flipStackFromIndex(stack,i);
                            viewModel.requiredFlipsArray[index] = viewModel.requiredFlipsArray[index] + 1;
                        }
                    }

                    //final flip from the bottom
                    viewModel.requiredFlipsArray[index] = viewModel.requiredFlipsArray[index] + 1;
                }

                //if the bottom pancake is face up then we flip pancakes from the bottom up
                else if(stack[lengthOfStack-1]=="+"){

                    //instead of starting from i-1 we start from i-2 since the first case is already accounted for
                    for(var i=lengthOfStack-2;i>=0;i--){

                        if(stack[i]=="-"){
                            stack = _flipStackFromIndex(stack,i);
                            viewModel.requiredFlipsArray[index] = viewModel.requiredFlipsArray[index] + 1;
                        }
                    }
                }
            }
        };

        viewModel.showPancakeTable = function(){

            return viewModel.pancakesTestCasesArray.length > 0;
        };

        //private helper functions

        function _getPancakeStackFromIndex(array,index){

            var stack = "";

            if(angular.isDefined(array) && angular.isDefined(index)){

                stack = array[index];
                return stack;
            }

            else{

                return null
            }

        }

        function _flipStackFromIndex(stackArray,index){

            var flippedStack = stackArray;

            for(var i=index;i>=0;i--){

                if(flippedStack[i]=="+"){

                    flippedStack[i]="-";
                }

                else if(flippedStack[i]=="-"){

                    flippedStack[i]="+";
                }
            }

            return flippedStack;

        }

    }

}());